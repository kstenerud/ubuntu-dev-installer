#!/bin/bash
set -eu

copy_directory_contents()
{
	src_dir=$1
	dst_dir=$2
	shopt -s dotglob
	rsync -a ${src_dir}/* ${dst_dir}/
}

map_path()
{
	old_path="$1"
	new_path="$2"
	copy_contents=$3

	mkdir -p "${new_path}"

	if [ -d "$old_path" ]; then
		if [ "$copy_contents" == "true" ]; then
			echo "Copying contents of $old_path to $new_path..."
			copy_directory_contents "$old_path" "$new_path"
		fi
		echo "Backing up existing ${old_path} to ${old_path}.bak"
		mv "${old_path}" "${old_path}.bak"
	fi

	mkdir "${old_path}"
	echo "Backing up /etc/fstab to /etc/fstab.bak"
	cp /etc/fstab /etc/fstab.bak
	echo "$new_path $old_path none bind 0 0" | tee -a /etc/fstab
	mount -a
}

show_help()
{
    echo "Bind mounts an old path to a new path

Usage: $(basename $0) [options] <old path> <new path>
Options:
    -c: Copy contents of old path to new path before mapping"
}

usage()
{
    show_help 1>&2
    exit 1
}

#####################################################################

COPY_CONTENTS=false

while getopts "?c" o; do
    case "$o" in
        \?)
            show_help
            exit 0
            ;;
        c)
			COPY_CONTENTS=true
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))


if [ $# -ne 2 ]; then
	usage
fi

OLD_PATH=$1
NEW_PATH=$2

map_path "$OLD_PATH" "$NEW_PATH" $COPY_CONTENTS

echo "Mapped $OLD_PATH to $NEW_PATH."
